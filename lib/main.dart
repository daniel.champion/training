import 'package:flutter/material.dart';
import 'ghflutter.dart';
import 'theme.dart';

MyTheme currentTheme = MyTheme();

void main() => runApp(const GHFlutterApp());

class GHFlutterApp extends StatefulWidget {
  static final ValueNotifier<ThemeMode> themeNotifier =
      ValueNotifier(ThemeMode.light);
  const GHFlutterApp({Key? key}) : super(key: key);

  @override
  State<GHFlutterApp> createState() => _GHFlutterAppState();
}

class _GHFlutterAppState extends State<GHFlutterApp> {
  @override
  void initState() {
    super.initState();
    currentTheme.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<ThemeMode>(
        valueListenable: GHFlutterApp.themeNotifier,
        builder: (_, ThemeMode currentMode, __) {
          return MaterialApp(
            title: 'GHFlutter',
            //theme: ThemeData(primarySwatch: Colors.brown),
            theme: ThemeData.light(),
            darkTheme: ThemeData.dark(),
            themeMode: currentTheme.currentTheme(),
            home: const GHFlutter(),
          );
        });
  }
}
