import 'package:flutter/material.dart';
import 'classes.dart';
import 'package:url_launcher/url_launcher.dart';

void launchUrl(String _url) async {
  if (!await launch(_url)) throw 'Could not launch $_url';
}

class MemberWidget extends StatefulWidget {
  final Member member;

  MemberWidget(this.member) {
    if (member == null) {
      throw ArgumentError(
          "member of MemberWidget cannot be null. Received: '$member'");
    }
  }

  @override
  _MemberWidgetState createState() => _MemberWidgetState(member);
}

class PhotoHero extends StatelessWidget {
  const PhotoHero({ Key? key, this.photo, this.onTap, this.width }) : super(key: key);

  final String? photo;
  final VoidCallback? onTap;
  final double? width;

  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: Hero(
        tag: photo!,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: onTap,
            child: Image.asset(
              photo!,
              fit: BoxFit.contain,
            ),
          ),
        ),
      ),
    );
  }
}

class HeroAnimation extends StatelessWidget {
  Widget build(BuildContext context) {
    final timeDilation = 5.0; // 1.0 means normal animation speed.

    return Scaffold(
      appBar: AppBar(
        title: const Text('Basic Hero Animation'),
      ),
      body: Center(
        child: PhotoHero(
          photo: 'images/flippers-alpha.png',
          width: 300.0,
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute<void>(
                builder: (BuildContext context) {
                  return Scaffold(
                    appBar: AppBar(
                      title: const Text('Flippers Page'),
                    ),
                    body: Container(
                      // The blue background emphasizes that it's a new route.
                      color: Colors.lightBlueAccent,
                      padding: const EdgeInsets.all(16.0),
                      alignment: Alignment.topLeft,
                      child: PhotoHero(
                        photo: 'images/flippers-alpha.png',
                        width: 100.0,
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  );
                }
            ));
          },
        ),
      ),
    );
  }
}

class _MemberWidgetState extends State<MemberWidget> {
  final Member member;

  _MemberWidgetState(this.member);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(onPressed: () {
          Navigator.pop(context);
        }),
        title: Text(member.login ?? "Member"),
      ),
      body: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(children: [
            Image.network(member.avatarUrl!),
            ElevatedButton(
                child: Text('Back'),
                onPressed: () {
                  Navigator.pop(context);
                }
            ),
            Text(member.type ?? "Member"),
            Text(member.id ?? "Member"),
            GestureDetector(
                child: Text((member.url ?? "Member")+"\n"),
                // 4, 5
                onTap: () {
                  launchUrl(member.url.toString());
                }),
            ElevatedButton(
                child: Text("Press Here"),
                onPressed: () {
                  _showOKScreen(context);
                })
          ])));
  }

  _showOKScreen(BuildContext context) async {
    // 1, 2
    bool? value = await Navigator.push(
      context,
      MaterialPageRoute<bool>(
        builder: (BuildContext context) {
          return Scaffold(
            body: Padding(
              padding: const EdgeInsets.all(32.0),
              // 3
              child: Column(
                children: [
                  Text("\n\n\nPLS PRESS \"OK\" OR \"NOT OK\"\n\n"),
                  GestureDetector(
                      child: Text('OK'),
                      // 4, 5
                      onTap: () {
                        Navigator.pop(context, true);
                      }),
                  Text("\n"),
                  GestureDetector(
                    child: Text('NOT OK'),
                    // 4, 5
                    onTap: () {
                      Navigator.pop(context, false);
                    },
                  ),
                  Text("\n                       _. - ~ ~ ~ - .\n    ..       __.    .-~               ~-.\n    ((\\     /   `}.~                     `.\n\\\\\\   {     }               /     \\   \\\n(\   \\\\~~^      }              |       }   \\\n\\`.-~ -@~      }  ,-.         |       )    \\\n(___     ) _}   (    :        |    / /      `.\n`----._-~.     _\\ \\ |_       \   / /- _      `.\n~~----~~  \\ \| ~~--~~~(  + /     ~-.     ~- _\n/  /         \  \\          ~- . _ _~_-_.\n__/  /          _\\  )\n    .<___.'         .<___/"),
                ],
              ),
            ),
          );
        },
      ),
    );
    // 6
    var alert = AlertDialog(
      content: Text((value != null && value) ? "OK was pressed" : "NOT OK or BACK was pressed"),
      actions: <Widget>[
        TextButton(
            child: Text('OK'),
            // 7
            onPressed: () {
              Navigator.pop(context);
            })
      ],
    );
    // 8
    showDialog(context: context, builder: (context) => alert);
  }
}
