import 'dart:convert';
import 'package:danchampion/main.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'classes.dart';
import 'memberWidget.dart';
import 'dart:math';

String image = '';

class GHFlutter extends StatefulWidget {
  const GHFlutter({Key? key}) : super(key: key);
  @override
  _GHFlutterState createState() => _GHFlutterState();
}

class _GHFlutterState extends State<GHFlutter> {
  final _members = <Member>[];
  final _news = <News>[];
  final _biggerFont = const TextStyle(fontSize: 21.0);

  Future<void> _loadData() async {
    const dataUrl = 'https://api.github.com/orgs/raywenderlich/members';
    final response = await http.get(Uri.parse(dataUrl));
    setState(() {
      final dataList = json.decode(response.body) as List;
      for (final item in dataList) {
        final login = item['login'] as String? ?? '';
        //final avatarUrl = "https://avatars.githubusercontent.com/u/2422207?v=4";
        final avatarUrl = item['avatar_url'] as String? ?? '';
        final type = item['type'] as String? ?? '';
        final id = item['id'].toString() as String? ?? '';
        final url = item['url'] as String? ?? '';
        final member = Member(login, avatarUrl, type, id, url);
        //print('spinach');
        _members.add(member);
      }
    });
  }

  Future<void> _loadNews() async {
    List<String> n = [];
    const newsUrl = 'https://hacker-news.firebaseio.com/v0/topstories.json';
    final news = await http.get(Uri.parse(newsUrl));
    String newsArticle = '';
    setState(() {
      final newsList = json.decode(news.body) as List;
      for (final item in newsList) {
        String p = item.toString() as String? ?? '';
        newsArticle = "https://hacker-news.firebaseio.com/v0/item/" + p + ".json";
        //print(newsArticle);
        n.add(newsArticle);
      }
      //print(n);
    });
    for (String i in n) {
      //print(i);
      final newNews = await http.get(Uri.parse(i));
      setState(() {
        var newNewsNewNew = json.decode(newNews.body);

        final author = newNewsNewNew["by"] as String? ?? '';
        final headline = newNewsNewNew["title"] as String? ?? '';
        final url = newNewsNewNew["url"] as String? ?? '';
        //final imageUrl = "https://picsum.photos/400/300" as String? ?? '';

        _news.add(News(author,headline,url));
        //print(News(author).toString());

        /* final newNewsList = json.decode(newNews.body)['by'] as List; ////////PROBLEMO
        for (final item in newNewsList) {
          final author = item["by"] as String? ?? '';
          //final headline = item["title"] as String? ?? '';
          //final url = item["url"] as String? ?? '';
          final news = News(author);
          _news.add(news);
          //print("AAAAAAA");
        }*/

      });
    }
  }
    _pushMember(Member member) {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => MemberWidget(member)));
    }

    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Daniel Champion Application"),
        ),
        body: ListView.separated(
            //scrollDirection: Axis.horizontal,
            itemCount: _members.length,
            itemBuilder: (BuildContext context, int position) {
              return _buildRow(position);
            },
            separatorBuilder: (context, index) {
              return const Divider();
            }),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FloatingActionButton(
                heroTag: 5,
                  onPressed: () {
                    currentTheme.switchTheme();
                  },
                  child: const Icon(Icons.airline_seat_legroom_normal_rounded)),
            ),
            FloatingActionButton(
                onPressed: () {
                  image = "https://picsum.photos/400/300";
                  _showNews(context,_news);
                },
                child: const Icon(Icons.videogame_asset)),
          ],
        ),
      );
    }

    @override
    void initState() {
      super.initState();
      _loadData();
      _loadNews();
    }

    Widget _buildRow(int i) {
      return Padding(
          padding: const EdgeInsets.all(21.0),
          child: ListTile(
            title: Text((_members[i].login ?? "Member").toString().toUpperCase(),
                style: _biggerFont),
            leading: CircleAvatar(
              backgroundColor: Colors.blue,
              backgroundImage:
              _members[i].avatarUrl == null ? null :
              NetworkImage(_members[i].avatarUrl ?? ""),
            ),
            // Add onTap here:
            onTap: () {
              _pushMember(_members[i]);
            },
          )
      );
    }
  }

_showNews(BuildContext context, final n) async {
  // 1, 2
  final News news;
  final _random = new Random();
  news = n[_random.nextInt(n.length)];

  bool? value = await Navigator.push(
    context,
    MaterialPageRoute<bool>(
      builder: (BuildContext context) {
        return Scaffold(
          appBar: AppBar(),
          body: Padding(
            padding: const EdgeInsets.all(32.0),
            // 3
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text(news.headline.toString(), style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 21),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    child: Text("\nBy " + news.author.toString() + "\n", style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(30.0),
                  child: Image.network("https://picsum.photos/400/300"),
                )
                Text("\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in rutrum est, et feugiat nisi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam eu orci at neque suscipit gravida in non lectus. Nam varius sed nunc vitae ultricies. Vivamus sagittis hendrerit condimentum. Morbi non tellus a quam posuere fringilla sed malesuada quam.\n"),
                GestureDetector(
                    child: Text('Read More'),
                    // 4, 5
                    onTap: () {
                      launchUrl(news.url.toString());
                      Navigator.pop(context,true);  // pop current page
                      Navigator.pushNamed(context, "Setting");
                    }),
                Text("\n"),
                GestureDetector(
                  child: Text('Back'),
                  // 4, 5
                  onTap: () {
                    Navigator.pop(context,false);  // pop current page
                    Navigator.pushNamed(context, "Setting");
                  },
                ),
              ],
            ),
          ),
        );
      },
    ),
  );
  // 6
  var alert = AlertDialog(
    content: Text((value != null && value) ? "Good news everyone!" : "Bad news everyone!"),
    actions: <Widget>[
      TextButton(
          child: Text('OK'),
          // 7
          onPressed: () {
            Navigator.pop(context);
          })
    ],
  );
  // 8
  showDialog(context: context, builder: (context) => alert);
}
