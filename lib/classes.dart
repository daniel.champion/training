class Member {
  Member(this.login, this.avatarUrl, this.type, this.id, this.url);
  final String? login;
  final String? avatarUrl;
  final String? type;
  final String? id;
  final String? url;
}

class News {
  News(this.author, this.headline, this.url);
  final String? author;
  final String? headline;
  final String? url;

  @override
  String toString(){
    return "AuthorID: $author";
  }
}
